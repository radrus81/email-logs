import { makeAutoObservable } from 'mobx'

class Pagination {
  page: number = 0
  rowsPerPage: number = 10

  constructor() {
    makeAutoObservable(this)
  }

  handleChangePage = (event: unknown, newPage: number) => {
    this.page = newPage
  }

  handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.rowsPerPage = +event.target.value
    this.page = 0
  }
}

export default new Pagination()
