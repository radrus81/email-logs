import { makeAutoObservable } from 'mobx'
import axios from 'axios'
import ShackbarsInfo from './shackbarsInfo'

class Login {
  userData = { isAuthorization: false, email: '' }

  constructor() {
    makeAutoObservable(this)
  }

  async login(email: string, password: string) {
    const authData = {
      email,
      password,
      returnSecureToken: true,
    }
    try {
      let response = await axios.post(
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDp4vezDg0kayvd6OpIEVUpuk5ognk_PIg',
        authData
      )
      this.setUserDataToLocalStorage(response.data)
    } catch (error) {
      ShackbarsInfo.showShackBar(error.response.data.error.message, 'error')
    }
  }

  logout() {
    localStorage.clear()
    this.userData = { isAuthorization: false, email: '' }
  }

  autoAuthorization() {
    const userData: any = {}
    for (let i = 0; i <= localStorage.length; i++) {
      let nameKey = localStorage.key(i)
      if (nameKey) {
        userData[nameKey] = localStorage.getItem(nameKey)
      }
    }
    if (Object.keys(userData).length) {
      const memoryExpirationDate: string | null = localStorage.getItem(
        'expirationDate'
      )

      if (memoryExpirationDate) {
        const expirationDate = new Date(memoryExpirationDate)

        if (expirationDate <= new Date()) {
          this.logout()
        } else {
          userData['isAuthorization'] = true
          this.userData = userData
        }
      }
    } else {
      this.logout()
    }
  }

  setUserDataToLocalStorage(userData: any) {
    const userKeys = Object.keys(userData)
    userKeys.forEach((key) => {
      localStorage.setItem(key, userData[key])
    })
    const expirationDate = new Date(
      new Date().getTime() + userData.expiresIn * 1000
    )
    localStorage.setItem('expirationDate', expirationDate.toString())
    userData['isAuthorization'] = true
    this.userData = userData
  }
}

export default new Login()
