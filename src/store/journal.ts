import { makeAutoObservable } from 'mobx'
import axios, { AxiosResponse } from 'axios'
import ShackbarsInfo from './shackbarsInfo'
import AddEditDialog from './AddEditDialog'

class Journal {
  rows: Array<any> = []

  constructor() {
    makeAutoObservable(this)
  }

  async getDataFromBase() {
    try {
      const response: AxiosResponse = await axios.get(
        `https://email-log-4d073-default-rtdb.europe-west1.firebasedatabase.app/journal.json`
      )
      this.rows = this.prepareData(response.data)
    } catch (error) {
      ShackbarsInfo.showShackBar(error, 'error')
    }
  }

  async setDataToBase(datas: any) {
    try {
      const response: AxiosResponse = await axios.post(
        `https://email-log-4d073-default-rtdb.europe-west1.firebasedatabase.app/journal.json`,
        datas
      )
      datas.id = response.data.name
      this.rows.unshift(datas)
      ShackbarsInfo.showShackBar('Add record', 'success')
      AddEditDialog.handleCloseAddEditDialog()
    } catch (error) {
      ShackbarsInfo.showShackBar(error, 'error')
    }
  }

  async editDataToBase(id: string, datas: any) {
    try {
      const response: AxiosResponse = await axios.put(
        `https://email-log-4d073-default-rtdb.europe-west1.firebasedatabase.app/journal/${id}.json`,
        datas
      )
      this.updateRecordInState(response.data)
      ShackbarsInfo.showShackBar('Edit record', 'success')
      AddEditDialog.handleCloseAddEditDialog()
    } catch (error) {
      ShackbarsInfo.showShackBar(error, 'error')
    }
  }

  async handleDeleteRecord(number: number) {
    let id: string = ''
    let indexRecord: number = 0
    this.rows.forEach((item: any, index: number): void => {
      if (item.number === number) {
        id = item.id
        indexRecord = index
      }
    })
    try {
      await axios.delete(
        `https://email-log-4d073-default-rtdb.europe-west1.firebasedatabase.app/journal/${id}.json`
      )
      this.rows.splice(indexRecord, 1)
      ShackbarsInfo.showShackBar('Delete record', 'success')
    } catch (error) {
      ShackbarsInfo.showShackBar(error, 'error')
    }
  }

  private prepareData(data: any): Array<string | number> {
    const arrPreparedData: Array<string | number> = []
    Object.entries(data).forEach((item: any, index: number): void => {
      item[1]['id'] = item[0]
      arrPreparedData.unshift(item[1])
    })
    return arrPreparedData
  }

  private updateRecordInState(data: any) {
    this.rows.forEach((item: any, index: number): void => {
      if (item.number === data.number) {
        data.id = item.id
        this.rows[index] = data
      }
    })
  }
}

export default new Journal()
