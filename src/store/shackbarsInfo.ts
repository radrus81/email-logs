import { makeAutoObservable } from 'mobx'

class ShackbarsInfo {
  isShowSnackbarsInfo: boolean = false
  message: string = ''
  messageStyle: string = 'info'

  constructor() {
    makeAutoObservable(this)
  }

  showShackBar(message: string, messageStyle: string) {
    this.isShowSnackbarsInfo = true
    this.message = message
    this.messageStyle = messageStyle
  }

  hideShackBar() {
    this.isShowSnackbarsInfo = false
    this.message = ''
    this.messageStyle = 'info'
  }
}

export default new ShackbarsInfo()
