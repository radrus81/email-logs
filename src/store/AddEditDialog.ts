import { makeAutoObservable } from 'mobx'
import JournalState from './journal'
import DataAuth from './login'

class AddEditDialog {
  isShowAddEditDialog: boolean = false
  dialogTitle: string = 'Add record'
  id: string = ''
  active: number = 0
  number: number = 0
  createdDate: string = ''
  sender: string = ''
  topic: string = ''
  user: string = ''

  constructor() {
    makeAutoObservable(this)
  }

  handleOpenAddEditDialog = (
    dialogTitle: string,
    number: number | null = null
  ) => {
    let dataRecord = this.getDataRecord(number)
    this.isShowAddEditDialog = true
    this.dialogTitle = dialogTitle
    this.active = !number ? this.getMaxNumber() : number
    this.number = !number ? this.getMaxNumber() : number
    this.id = dataRecord[0]
    this.createdDate = dataRecord[1]
    this.sender = dataRecord[2]
    this.topic = dataRecord[3]
    this.user = dataRecord[4]
  }

  handleCloseAddEditDialog = () => {
    this.isShowAddEditDialog = false
  }

  getMaxNumber(): number {
    let memoryNumber = 0
    JournalState.rows.forEach((item: any) => {
      if (memoryNumber < item.number) {
        memoryNumber = item.number
      }
    })
    return ++memoryNumber
  }

  getDataRecord(number: number | null): Array<string> {
    let id: string = ''
    let createdDate: string
    let user: string
    let sender: string = ''
    let topic: string = ''
    if (!number) {
      let date = new Date()
      let day = date.getDate()
      let dayStr = day < 10 ? `0${day}` : day
      let month = date.getMonth() + 1
      let monthStr = day < 10 ? `0${month}` : month
      createdDate = `${dayStr}.${monthStr}.${date.getFullYear()}`
      user = DataAuth.userData.email
    } else {
      let data: any = JournalState.rows.filter((item: any) => {
        return item.number === number
      })
      id = data[0].id
      createdDate = data[0].createdDate
      user = data[0].user
      sender = data[0].sender
      topic = data[0].topic
    }
    return [id, createdDate, sender, topic, user]
  }
}

export default new AddEditDialog()
