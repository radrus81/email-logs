import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import Authorization from '../components/Authorization/Authorization'
import Journal from '../components/Journal/Journal'

interface PropsRoutes {
  isAuthorization: boolean
}

const Routes: React.FC<PropsRoutes> = ({ isAuthorization }) => {
  return (
    <div className="container">
      {isAuthorization ? (
        <Switch>
          <Route component={Journal} path="/" />
          <Redirect to="/" />
        </Switch>
      ) : (
        <Switch>
          <Route component={Authorization} path="/" exact />
          <Redirect to="/" />
        </Switch>
      )}
    </div>
  )
}

export default Routes
