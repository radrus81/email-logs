import React from 'react'
import {
  AppBar,
  Toolbar,
  Typography,
  IconButton,
  Box,
  // ListItemText,
  // ListItem,
} from '@material-ui/core/'
import { /*NavLink,*/ RouteComponentProps, withRouter } from 'react-router-dom'
import useStyles from './NavbarStyles'
import ProfileButton from './ProfileButton'

interface PropsNavBar {
  isAuthorization: boolean
  email: string
  logout: Function
}

// const preventDefault = (event: React.SyntheticEvent) => event.preventDefault()

const Navbar: React.FC<PropsNavBar & RouteComponentProps> = ({
  isAuthorization,
  email,
  logout,
  location,
}) => {
  const classes = useStyles()

  // const sections = [{ title: 'Journal', url: '/journal' }]
  return (
    <Box className={classes.root}>
      <AppBar position="static">
        <Toolbar>
          <IconButton
            edge="start"
            className={classes.menuButton}
            color="inherit"
            aria-label="menu"
          ></IconButton>
          <Typography variant="h6" className={classes.title}>
            Email Logs
          </Typography>

          {isAuthorization ? (
            <>
              {/* <Toolbar
                component="nav"
                variant="dense"
                className={classes.toolbarSecondary}
              >
                {sections.map(({ url, title }) => (
                  <ListItem
                    button
                    key={title}
                    component={NavLink}
                    to={url}
                    selected={location.pathname === url ? true : false}
                  >
                    <ListItemText primary={title} />
                  </ListItem>
                ))}
              </Toolbar> */}
              <ProfileButton email={email} logout={logout} />
            </>
          ) : null}
        </Toolbar>
      </AppBar>
    </Box>
  )
}

export default withRouter(Navbar)
