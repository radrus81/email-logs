import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginBottom: theme.spacing(2),
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    toolbarSecondary: {
      justifyContent: 'space-between',
      overflowX: 'auto',
      marginTop: '3px',
    },
    toolbarLink: {
      padding: theme.spacing(1),
      flexShrink: 0,
      color: 'inherit',
      textDecoration: 'none',
    },
  })
)

export default useStyles
