import React from 'react'
import {
  Paper,
  Button,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TablePagination,
  TableRow,
} from '@material-ui/core/'
import { PostAdd } from '@material-ui/icons/'
import { observer } from 'mobx-react-lite'
import useStyles from './JournalStyles'
import TableHeader from './TableHeader'
import columns from './Columns'
import ActiveButtons from './ActiveButtons'
import JournalState from '../../store/journal'
import Pagination from '../../store/pagination'
import AddEditDialogStore from '../../store/AddEditDialog'

const Journal: React.FC = observer(() => {
  const classes = useStyles()
  const { page, rowsPerPage } = Pagination
  const { rows } = JournalState

  return (
    <Paper className={classes.root}>
      <Button
        variant="contained"
        color="primary"
        className={classes.button}
        startIcon={<PostAdd />}
        onClick={() => {
          AddEditDialogStore.handleOpenAddEditDialog('Add record')
        }}
      >
        Add new record
      </Button>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHeader />
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row: any) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                    {columns.map((column) => {
                      const value = row[column.id]
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.id === 'active' ? (
                            <ActiveButtons number={value} />
                          ) : (
                            value
                          )}
                        </TableCell>
                      )
                    })}
                  </TableRow>
                )
              })}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onChangePage={Pagination.handleChangePage}
        onChangeRowsPerPage={Pagination.handleChangeRowsPerPage}
      />
    </Paper>
  )
})

export default Journal
