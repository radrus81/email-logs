import { makeStyles } from '@material-ui/core/styles'

const JournalStyles = makeStyles((theme) => ({
  root: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
  },
  container: {
    maxHeight: 440,
    marginTop: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(1),
  },
}))

export default JournalStyles
