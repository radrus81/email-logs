import React from 'react'
import { Create, Delete } from '@material-ui/icons'
import IconButton from '@material-ui/core/IconButton'
import AddEditDialogStore from '../../store/AddEditDialog'
import JournalStore from '../../store/journal'

const ActiveButtons: React.FC<any> = ({ number }) => {
  return (
    <>
      <IconButton
        aria-label="expand row"
        size="small"
        onClick={() => {
          AddEditDialogStore.handleOpenAddEditDialog('Edit record', number)
        }}
      >
        <Create />
      </IconButton>
      <IconButton
        aria-label="expand row"
        size="small"
        onClick={() => {
          JournalStore.handleDeleteRecord(number)
        }}
      >
        <Delete />
      </IconButton>
    </>
  )
}

export default ActiveButtons
