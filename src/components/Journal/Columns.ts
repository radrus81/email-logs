interface Column {
  id: 'active' | 'number' | 'createdDate' | 'sender' | 'topic' | 'user'
  label: string
  minWidth?: number
  align?: 'center'
}

const columns: Column[] = [
  { id: 'active', label: '', minWidth: 70 },
  { id: 'number', label: 'Number', minWidth: 50 },
  {
    id: 'createdDate',
    label: 'Date',
    minWidth: 100,
    align: 'center',
  },
  {
    id: 'sender',
    label: 'Sender',
    minWidth: 100,
    align: 'center',
  },
  {
    id: 'topic',
    label: 'Topic',
    minWidth: 270,
    align: 'center',
  },
  {
    id: 'user',
    label: 'User',
    minWidth: 170,
    align: 'center',
  },
]

export default columns
