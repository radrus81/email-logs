import React from 'react'
import { Container, CssBaseline, Box } from '@material-ui/core/'
import { Form, FormikProps } from 'formik'
import useStyles from '../AuthorizationStyles'
import FormikField from './FormikField'
import FormikButton from './FormikButton'
import FormikHeader from './FormikHeader'

interface FormValues {
  email: string
  password: string
}

interface OtherProps {}

export const AuthForm = (props: OtherProps & FormikProps<FormValues>) => {
  const classes = useStyles()

  const { values, errors, touched, handleSubmit, isSubmitting } = props

  return (
    <Container>
      <CssBaseline />
      <Box className={classes.paper}>
        <FormikHeader title="Sign in" />
        <Form onSubmit={handleSubmit}>
          <FormikField
            error={errors.email}
            touch={touched.email}
            value={values.email}
            name="email"
            label="E-mail"
            type="text"
          />
          <FormikField
            error={errors.password}
            touch={touched.password}
            value={values.password}
            name="password"
            type="password"
            label="Password"
          />
          <FormikButton
            classes={classes}
            isSubmitting={isSubmitting}
            btnTitle="Sing In"
          />
        </Form>
      </Box>
    </Container>
  )
}
