import React from 'react'
import { Field } from 'formik'
import { TextField } from 'material-ui-formik-components/TextField'

interface PropsField {
  error: string | undefined
  touch: boolean | undefined
  value: string
  name: string
  type: string
  label: string
}

const FormikField: React.FC<PropsField> = ({
  error,
  touch,
  value,
  name,
  type,
  label,
}) => {
  return (
    <Field
      component={TextField}
      error={error && touch}
      variant="outlined"
      margin="normal"
      fullWidth
      name={name}
      label={label}
      type={type}
      id={name}
      value={value || ''}
    />
  )
}

export default FormikField
