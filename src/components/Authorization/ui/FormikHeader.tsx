import React from 'react'
import Avatar from '@material-ui/core/Avatar'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import useStyles from '../AuthorizationStyles'

type PropsFormikHeader = {
  title: string
}

const FormikHeader: React.FC<PropsFormikHeader> = ({ title }) => {
  const classes = useStyles()

  return (
    <>
      <Avatar className={classes.avatar}>
        <LockOutlinedIcon />
      </Avatar>
      <Typography component="h1" variant="h5">
        {title}
      </Typography>
    </>
  )
}

export default FormikHeader
