import React from 'react'
import Button from '@material-ui/core/Button'

interface PropsFormikButton {
  classes: { submit: string }
  isSubmitting: true | false
  btnTitle: string
}

const FormikButton: React.FC<PropsFormikButton> = ({
  classes,
  isSubmitting,
  btnTitle,
}) => {
  return (
    <Button
      type="submit"
      fullWidth
      variant="contained"
      color="primary"
      className={classes.submit}
      disabled={isSubmitting}
    >
      {btnTitle}
    </Button>
  )
}

export default FormikButton
