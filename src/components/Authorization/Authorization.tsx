import { withFormik } from 'formik'
import AuthorizationFormSchema from './AuthorizationFormSchema'
import { AuthForm } from './ui/AuthForm'
import Login from '../../store/login'

interface FormValues {
  email: string
  password: string
}

interface AuthFormProps {
  initialEmail?: string
  initialPassword?: string
}

const Authorization = withFormik<AuthFormProps, FormValues>({
  mapPropsToValues: (props) => ({
    email: props.initialEmail || '',
    password: props.initialPassword || '',
  }),

  validationSchema: AuthorizationFormSchema,

  handleSubmit(
    { email, password }: FormValues,
    { props, setSubmitting, setErrors }
  ) {
    Login.login(email, password)
    setSubmitting(false)
  },
})(AuthForm)

export default Authorization
