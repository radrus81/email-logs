import * as Yup from 'yup'

const AuthorizationFormSchema = Yup.object().shape({
  email: Yup.string().email('Enter a valid email').required('Login required'),
  password: Yup.string()
    .min(6, 'The password must be 6 characters long')
    .required('Password required'),
})

export default AuthorizationFormSchema
