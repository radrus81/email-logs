import React from 'react'
import { Box, Dialog, DialogContent, DialogTitle } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import AddEditDialogStore from '../../../store/AddEditDialog'
import AddEditFormik from './AddEditFormik'

const AddEditDialog: React.FC = observer(() => {
  const { isShowAddEditDialog, dialogTitle } = AddEditDialogStore

  return (
    <Box>
      <Dialog
        open={isShowAddEditDialog}
        onClose={AddEditDialogStore.handleCloseAddEditDialog}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">{dialogTitle}</DialogTitle>
        <DialogContent>
          <AddEditFormik />
        </DialogContent>
      </Dialog>
    </Box>
  )
})

export default AddEditDialog
