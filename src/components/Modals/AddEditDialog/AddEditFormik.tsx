import React from 'react'
import { useFormik } from 'formik'
import * as yup from 'yup'
import { Box, Button, DialogActions, TextField } from '@material-ui/core'
import { observer } from 'mobx-react-lite'
import AddEditDialogStore from '../../../store/AddEditDialog'
import JournalStore from '../../../store/journal'

const validationSchema = yup.object({
  sender: yup.string().required('sender is required'),
  topic: yup.string().required('topic is required'),
})

const AddEditFormik: React.FC<{}> = observer(() => {
  const {
    id,
    dialogTitle,
    active,
    number,
    createdDate,
    sender,
    topic,
    user,
  } = AddEditDialogStore
  const formik = useFormik({
    initialValues: {
      active,
      number,
      createdDate,
      sender,
      topic,
      user,
    },
    validationSchema: validationSchema,
    onSubmit: (values) => {
      values['active'] = values.number
      if (dialogTitle === 'Add record') {
        JournalStore.setDataToBase(values)
      } else {
        JournalStore.editDataToBase(id, values)
      }
    },
  })

  const { values, touched, errors, handleChange } = formik

  return (
    <Box>
      <form onSubmit={formik.handleSubmit}>
        <TextField
          fullWidth
          id="sender"
          name="sender"
          label="Sender"
          value={values.sender}
          onChange={handleChange}
          error={touched.sender && Boolean(errors.sender)}
          helperText={touched.sender && errors.sender}
        />
        <TextField
          fullWidth
          id="topic"
          name="topic"
          label="Topic"
          value={values.topic}
          onChange={handleChange}
          error={touched.topic && Boolean(errors.topic)}
          helperText={touched.topic && errors.topic}
        />
        <DialogActions>
          <Button
            onClick={AddEditDialogStore.handleCloseAddEditDialog}
            color="primary"
          >
            Cancel
          </Button>
          <Button type="submit" color="primary">
            {dialogTitle}
          </Button>
        </DialogActions>
      </form>
    </Box>
  )
})

export default AddEditFormik
