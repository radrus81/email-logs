import React from 'react'
import Snackbar from '@material-ui/core/Snackbar'
import MuiAlert from '@material-ui/lab/Alert'
import { observer } from 'mobx-react-lite'
import { makeStyles } from '@material-ui/core/styles'
import ShackbarsInfo from '../../store/shackbarsInfo'

function Alert(props: any) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
}))

const SnackbarsInfo: React.FC = observer(() => {
  const { isShowSnackbarsInfo, messageStyle, message } = ShackbarsInfo
  const classes = useStyles()

  const handleHide = () => {
    ShackbarsInfo.hideShackBar()
  }
  return (
    <div className={classes.root}>
      <Snackbar
        open={isShowSnackbarsInfo}
        autoHideDuration={5000}
        onClose={handleHide}
      >
        <Alert onClose={handleHide} severity={messageStyle}>
          {message}
        </Alert>
      </Snackbar>
    </div>
  )
})

export default SnackbarsInfo
