import React, { useEffect } from 'react'
import { Box } from '@material-ui/core/'
import { BrowserRouter } from 'react-router-dom'
import { observer } from 'mobx-react-lite'
import NavBar from './components/Navbar/Navbar'
import Routes from './containers/Routes'
import Login from './store/login'
import ShackbarsInfo from './components/Modals/SnackbarsInfo'
import AddEditDialog from './components/Modals/AddEditDialog/AddEditDialog'
import JournalState from './store/journal'

const App: React.FC = observer(() => {
  const { isAuthorization, email } = Login.userData
  useEffect(() => {
    Login.autoAuthorization()
    JournalState.getDataFromBase()
  }, [])

  return (
    <BrowserRouter>
      <Box className="App">
        <NavBar
          isAuthorization={isAuthorization}
          email={email}
          logout={() => {
            Login.logout()
          }}
        />
        <ShackbarsInfo />
        <AddEditDialog />
        <Routes isAuthorization={isAuthorization} />
      </Box>
    </BrowserRouter>
  )
})

export default App
